PREFIX=/usr/local
W3M := $(shell command -v w3m 2> /dev/null)

.PHONY: install
install: $(PREFIX)/bin/kvartersmenyn

.PHONY: uninstall
uninstall:
	$(RM) $(PREFIX)/bin/kvartersmenyn

$(PREFIX)/bin/kvartersmenyn: kvartersmenyn.sh
ifndef W3M
	$(error Missing dependency: w3m)
endif
	install -m 755 $< $@
