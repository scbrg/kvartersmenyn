#!/bin/sh

set -C
set -e
set -f
set -u

URL="http://www.kvartersmenyn.se/rest"
RESTAURANT="${1}"

if [ "$1" = - ]; then
    cat
else
    w3m -dump "${URL}"/"${RESTAURANT}"
fi | awk '
/VECKA/ {
    menu=1
}
/PRIS:/ {
    menu=0
}
{
    if(menu) {
        print
    }
}
'
